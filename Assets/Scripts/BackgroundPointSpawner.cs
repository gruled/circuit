﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BackgroundPointSpawner : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    [SerializeField] private int count;
    [SerializeField] private float delay;
    private RectTransform me;
    private float _halfRectWidth;
    private float _rectWidth;

    private void Start()
    {
        me = transform.GetComponent<RectTransform>();
        _rectWidth = me.rect.width - 10.0f;
        _halfRectWidth = _rectWidth / 2;
        StartCoroutine(Spawner());
    }

    private IEnumerator Spawner()
    {
        while (true)
        {
            RectTransform rectTransform = Instantiate(prefab, transform).GetComponent<RectTransform>();
            float progress = Random.value;
            rectTransform.anchoredPosition = new Vector2((_rectWidth * progress) - _halfRectWidth, -10.0f);
            yield return new WaitForSeconds(delay);
        }
    }
}
