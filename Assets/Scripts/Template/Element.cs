﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Element : MonoBehaviour, IPointerClickHandler
{
    public bool State;
    public List<Element> Input;
    public List<Element> Output;

    public virtual void ReCalc()
    {
        
    }

    public void Callback()
    {
        foreach (var item in Output)
        {
            item.ReCalc();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Tap");
        State = !State;
        Callback();
    }

    private void Start()
    {
        for (int i = 0; i < Output.Count; i++)
        {
            LineRenderer lineRenderer = gameObject.AddComponent<LineRenderer>();
            lineRenderer.widthMultiplier = 0.05f;
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, Output[i].transform.position);
        }
    }
}
