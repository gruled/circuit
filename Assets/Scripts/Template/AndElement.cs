﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndElement : Element
{
    public override void ReCalc()
    {
        Debug.Log($"{transform} callback");
        foreach (var item in Input)
        {
            Debug.Log($"Input {item}  -- state: {item.State}");
        }
        if (Input.Count==1)
        {
            State = Input[0];
        }

        if (Input.Count>1)
        {
            bool result = true;
            for (int i = 0; i < Input.Count; i++)
            {
                if (Input[i]==false)
                {
                    result = false;
                    break;
                }
            }

            State = result;
        }
        
        Debug.Log($"Count: {Input.Count}");
        Callback();
    }
}
