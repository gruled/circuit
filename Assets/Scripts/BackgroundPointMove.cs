﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class BackgroundPointMove : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float lifetime;
    private RectTransform me;

    private void Start()
    {
        me = GetComponent<RectTransform>();
        StartCoroutine(destroyer());
    }

    private void Update()
    {
        me.anchoredPosition += Vector2.up * speed * Time.deltaTime;
    }

    private IEnumerator destroyer()
    {
        yield return new WaitForSeconds(lifetime);
        Destroy(gameObject);
    }
}
